import ListCart from '../component/cart/listCart'
import MainLayoutUser from '../component/mainLayotUser'

export default function Beranda() {
    return (
        <MainLayoutUser>
            <ListCart />
        </MainLayoutUser>
    )
}
