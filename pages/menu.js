import 'antd/dist/antd.css'
import ListMenu from '../component/menu/listMenu'
import MainLayoutUser from '../component/mainLayotUser'

export default function Beranda() {
    return (
        <MainLayoutUser>
            <ListMenu />
        </MainLayoutUser>
    )
}
