import KontenUsers from "../../component/admin/konten/user"
import MainLayout from "../../component/admin/layout/index";



export default function DasboardUsers() {

    return (
        <div>
            <MainLayout>
                <KontenUsers />
            </MainLayout>
        </div>
    )
}