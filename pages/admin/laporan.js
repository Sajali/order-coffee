import KontenLaporan from "../../component/admin/konten/laporan";
import MainLayout from "../../component/admin/layout/index";



export default function DasboardLaporan() {

    return (
        <div>
            <MainLayout>
                <KontenLaporan />
            </MainLayout>
        </div>
    )
}