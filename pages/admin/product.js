import KontenProduct from "../../component/admin/konten/product";
import MainLayout from "../../component/admin/layout/index";



export default function DasboardProduct() {

    return (
        <div>
            <MainLayout>
                <KontenProduct />
            </MainLayout>
        </div>
    )
}