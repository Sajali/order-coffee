import KontenTransaksi from "../../component/admin/konten/transaksi";
import MainLayout from "../../component/admin/layout/index";



export default function DasboardTransaksi() {

    return (
        <div>
            <MainLayout>
                <KontenTransaksi />
            </MainLayout>
        </div>
    )
}