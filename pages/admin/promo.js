import KontenPromo from "../../component/admin/konten/promo"
import MainLayout from "../../component/admin/layout/index";



export default function DasboardPromo() {

    return (
        <div>
            <MainLayout>
                <KontenPromo />
            </MainLayout>
        </div>
    )
}